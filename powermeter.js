
var settings, devices, templates;

function byId( myid ) {
  return $("#" + myid.replace( /( |:|\.|\[|\]|,|=|@)/g, "\\$1" ));
}

Handlebars.registerHelper('select', function( value, options ){
  var $el = $('<select />').html( options.fn(this) );
  $el.find('[value="' + value + '"]').attr({'selected':'selected'});
  return $el.html();
});

function settingsChangeHook(info) {
  info.find('input').on('input', function() {
    info.find('.x-btn').removeClass('disabled');
  });
}

function createInfo(settings, data) {
  var info = $(templates.settingsInfo(data));
  settingsChangeHook(info);
  info.submit('submit', function(event) {
    /* FIXME send update */
    event.preventDefault();
  });
  settings.find('.x-info').replaceWith(info);
}

function createCalib(settings, data) {
  var info = $(templates.settingsCalib(data));
  settingsChangeHook(info);
  info.submit('submit', function(event) {
    /* FIXME send update */
    event.preventDefault();
  });
  settings.find('.x-calib').replaceWith(info);
}

function createCfg(settings, data) {
  var info = $(templates.settingsConfig(data));
  var fields = info.find('input');
  var contrast = fields.eq(0);
  contrast.on('input', function() {
    contrast.siblings('.x-legend').text(contrast.val());
  });

  var power = fields.eq(1);
  power.on('input', function() {
    if (power.val() === '0') {
      power.siblings('.x-legend').text('Disabled');
    }
    else {
      power.siblings('.x-legend').text(power.val() + ' Seconds');
    }
  });
  power.trigger('input');
  settingsChangeHook(info);
  info.submit('submit', function(event) {
    /* FIXME send update */
    event.preventDefault();
  });
  settings.find('.x-config').replaceWith(info);
}

function onSettingsClicked(deviceName, deviceNumber) {
  var settings = $(templates.settings({ Name: deviceName, Number: deviceNumber }));
  settings.prop('id', 'settings');
  settings.on('hidden.bs.modal', function() {
    settings.modal('dispose');
    settings = null;
    devices.remove('#settings');
  });
  settings.on('shown.bs.modal', function () {
    settings.find('.close').trigger('focus');
  });
  settings.modal('show');

  function err(selector, jqXHR, message) {
    settings.find(selector)
    .replaceWith(templates.error({ message: message }));
  }

  $.ajax('GetInfo', { data: { device: deviceNumber } })
  /* send fake values for debugging purpose */
  .catch(function() {
    return { UniqueId: 123, Sensor: 'test', Resistor: 'test' };
  })
  .then(createInfo.bind(null, settings), err.bind(null, '.x-info'));

  $.ajax('GetCal', { data: { device: deviceNumber } })
  /* send fake values for debugging purpose */
  .catch(function() {
    return { VL: 12, VU: 23, Ugain: 34, Igain: 45, MC: 'solar', UN: 67, IB: 89,
      Cal_Lgain: 16, Cal_Ngain: 2 };
  })
  .then(createCalib.bind(null, settings), err.bind(null, '.x-calib'));

  $.ajax('GetCfg', { data: { device: deviceNumber } })
  /* send fake values for debugging purpose */
  .catch(function() {
    return { Contrast: 5, PowerSave: 3, BoardName: 'Happy Calamari' };
  })
  .then(createCfg.bind(null, settings), err.bind(null, '.x-config'));
}

function onSettingsSubmit() {
  var device = byId(settings.find('.x-title').text());
  var btn = device.find('.x-settingsButton');
  btn.addClass('fa-spin');
  btn.addClass('text-danger');
  settings.modal('hide');
}

function createDevice(data) {
  data.Connected = (parseInt(data.Connected, 10) === 1);
  var dev = $(templates.device(data));
  devices.append(dev);
}

$(document).ready(function() {
  settings = $('#settings');
  devices = $('#devices');
  templates = {
    device: Handlebars.compile(document.getElementById('t-device').innerHTML),
    settings: Handlebars.compile(document.getElementById('t-settings').innerHTML),
    settingsInfo: Handlebars.compile(document.getElementById('t-info-settings').innerHTML),
    settingsConfig: Handlebars.compile(document.getElementById('t-config-settings').innerHTML),
    settingsCalib: Handlebars.compile(document.getElementById('t-calib-settings').innerHTML),
    error: Handlebars.compile(document.getElementById('t-error').innerHTML)
  };

  createDevice({
    Name: 'Device 12', Number: 12,
    Connected: '0',
    Volts: 123, Pmean: 456, Curr: 789, Qmean: 321, Freq: 654, Smean: 987
  });
  createDevice({
    Name: 'Device 42', Number: 42,
    Connected: '1',
    Volts: 123, Pmean: 456, Curr: 789, Qmean: 321, Freq: 654, Smean: 987
  });
});
