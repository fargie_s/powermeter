
const babel = require("@babel/core");

module.exports = {
  filters: {
    'babel': function(text, options) {
      return babel.transformSync(text, {
        presets: [ '@babel/preset-env', 'minify' ]
      }).code;
    }
  }
};
