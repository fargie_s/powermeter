# PowerMeter web-interface

Small draft application.

## Build

Assuming _Node.js_ and _NPM_ are already installed:
```bash
# Install required dependencies
npm install

# Build html file
npm run build
```
